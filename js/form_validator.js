/*

***
IMPLEMENT:

- html:
<form action="" method="POST" onsubmit="return validate_xxx()">

- js
function validate_xxx() {

	var rules = [
		{
			'id': '',									//	*id of the input	
			'name': '',									//	*the name of the input how it appears in the feedback
			'type': 'email|password|postal|num|phone',	//	*type of input
			'req': true|false,							//	*requested input
			'min': 8,									//	minimum length of the value
			'max': 8,									//	maximum length of the value
			'full_length': 8,							//	length of the value
			'equal': '',								//	the value must be the same as another input's value (other input's id)
		},
	];

	var options = {
		'inline': true
	};

	return validate_form(rules, options);
	
}

***


* requested data

*/

/* CONFIG */

var error_messages = {
	'req':			'\nA következő mező nem lehet üres: {name}',
	'email':		'\nA(z) "{name}" mező formátuma helytelen.',
	'num':			'\nA(z) "{name}" mező csak számokat tartalmazhat',
	'postal':		'\nA(z) "{name}" mező formátuma helytelen.',
	'full_length':	'\n A(z) "{name}" mezőnek pontosan {full_length} karakter hosszúnak kell lennie. (jelenleg {length})',
	'min':			'\n A(z) "{name}" mezőnek minimum {min} karakter hosszúnak kell lennie. (jelenleg {length})',
	'max':			'\n A(z) "{name}" mező maximum {max} karakter hosszú lehet. (jelenleg {length})',
	'equal':		'\nA "{name}" és a "{equal_name}" mezők tartalma nem egyezik meg.'
};

var error_messages_inline = {
	'req':			'\nA mező nem lehet üres.',
	'email':		'\nA mező formátuma helytelen.',
	'num':			'\nA mező csak számokat tartalmazhat',
	'postal':		'\nA mező formátuma helytelen.',
	'full_length':	'\n A mezőnek pontosan {full_length} karakter hosszúnak kell lennie. (jelenleg {length})',
	'min':			'\n A mezőnek minimum {min} karakter hosszúnak kell lennie. (jelenleg {length})',
	'max':			'\n A mező maximum {max} karakter hosszú lehet. (jelenleg {length})',
	'equal':		'\nA "{name}" és a "{equal_name}" mezők tartalma nem egyezik meg.'
};



/* /CONFIG */


function check_email(email) {

	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");


	if (atpos<1 || dotpos<atpos+2 || dotpos+2 >= email.length) {
		return false;
	} else {
		return true;
	};
	
}

function check_numeric(str) {

	var numericExpression = /^[0-9]+$/;

	if ( str.match(numericExpression) ) {

		return true;

	} else {

		return false;

	};

}

function check_length(val, method, length) {
	
	if ( method == 'min' && val.length < length ) {

		return false;

	} else if ( method == 'max' && val.length > length ) {

		return false;

	} else if ( method == 'length' && val.length !== length ) {

		return false;

	} else {
		return true;
	};

}

function validate_form(rules, options) {

	var error = '';

	var val;

	if ( typeof options.inline != undefined ) {

		$('.msg').empty();

	};

	$.each(rules, function() {

		val = $('#' + this.id).val();
		
		if (this.req == true && val == '' ) {				// required not empty


			if ( options.inline ) {

				msg = error_messages_inline.req;

				$( '#' + this.id + '_msg' ).html(msg);

				error = true;

			} else{

				msg = error_messages.req.replace('{name}', this.name);

				error += msg;

			};

		} else {

			//check type based rules
			if (this.type == 'email') {

				if ( !check_email(val) ) {					// email format is valid


					if ( options.inline ) {

						msg = error_messages_inline.email;

						$( '#' + this.id + '_msg' ).html(msg);

						error = true;

					} else{

						msg = error_messages.email.replace('{name}', this.name);

						error += msg;

					};
					
				};

			} else if ( this.type == 'num' ) {

				if ( !check_numeric(val) ) {				// check numeric


					if ( options.inline ) {

						msg = error_messages_inline.num;

						$( '#' + this.id + '_msg' ).html(msg);

						error = true;

					} else{
						
						msg = error_messages.num.replace('{name}', this.name);

						error += msg;

					};
					
				};
			} else if ( this.type == 'postal') {			// postal

				if ( val && ( !check_numeric(val) || !check_length(val, 'length', 4) ) ) {


					if ( options.inline ) {

						msg = error_messages_inline.postal;
						
						$( '#' + this.id + '_msg' ).html(msg);

						error = true;

					} else{

						msg = error_messages.postal.replace('{name}', this.name);

						error += msg;

					};

				};

			} else if( this.type == 'phone' ) {				//phone


			};

			// check length based rules
			if ( this.full_length && !check_length(val, 'length', this.full_length) ) {


				if ( options.inline ) {

					msg = error_messages_inline.full_length.replace('{full_length}', this.full_length).replace('{length}', val.length);

					$( '#' + this.id + '_msg' ).html(msg);

					error = true;

				} else{

					msg = error_messages.full_length.replace('{name}', this.name).replace('{full_length}', this.full_length).replace('{length}', val.length);

					error += msg;

				};


			} else if ( this.min && !check_length(val, 'min', this.min) ) {


				if ( options.inline ) {

					msg = error_messages_ineline.full_length.replace('{min}', this.min).replace('{length}', val.length);

					$( '#' + this.id + '_msg' ).html(msg);

					error = true;

				} else{

					msg = error_messages.full_length.replace('{name}', this.name).replace('{min}', this.min).replace('{length}', val.length);

					error += msg;

				};

			} else if ( this.max && !check_length(val, 'max', this.max) ) {


				if ( options.inline ) {

					msg = error_messages_inline.full_length.replace('{max}', this.max).replace('{length}', val.length);

					$( '#' + this.id + '_msg' ).html(msg);

					error = true;

				} else{

					msg = error_messages.full_length.replace('{name}', this.name).replace('{max}', this.max).replace('{length}', val.length);

					error += msg;

				};

			};

			//equal
			if ( this.equal ) {

				var compare = $('#' + this.equal).val;

				if ( val !== compare ) {


					if ( options.inline ) {

						msg = error_messages_inline.equal.replace('{name}', this.name).replace('{equal_name}', this.equal_name);

						$( '#' + this.id + '_msg' ).html(msg);

						error = true;

					} else{

						msg = error_messages.equal.replace('{name}', this.name).replace('{equal_name}', this.equal_name);

						error += msg;

					};
				};
			};
		};
	});

	if ( options.inline ) {

		if ( error ) {
			return false;
		} else{
			return true;
		};

	} else{
		
		if (error == '') {
			return true;
		} else {
			alert(error);
			return false;
		};

	};


}